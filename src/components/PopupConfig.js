export default {
    POPUP_CONF_DEFAULT: {
        dimensionClass: 'popup-600-800',
        displayCloseButton: true,
        coverBackground: 'darkgrey',
        coverOpacity: '95%',
        coverCloseOnClick: true
    },
    POPUP_CONF_BLANK_300_300: {
        dimensionClass: 'popup-300-300',
        displayCloseButton: false,
        coverBackground: 'white',
        coverOpacity: '100%',
        coverCloseOnClick: false
    }
}