import Util from './Util';
import Auth from './Auth';

const API_URL = 'https://frogaccelerator.com:8110';

const login = async (credentials) => {
    const response = await fetch(`${API_URL}/users/login`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(credentials)
    });
    return await Util.handleJsonResponse(response);
};

const getCompanies = async () => {
    const response = await fetch(`${API_URL}/companies`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${Auth.getToken()}`
        }
    });
    return await Util.handleJsonResponse(response);
};

const postFile = async file => {
    let formData = new FormData();
    formData.append("file", file);

    let response = await fetch(
        `${API_URL}/files/upload`,
        {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${Auth.getToken()}`
            },
            body: formData
        }
    );
    return await Util.handleJsonResponse(response);
};

const postCompany = async company => {
    let response = await fetch(
        `${API_URL}/companies`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${Auth.getToken()}`
            },
            body: JSON.stringify(company)
        }
    );
    Util.handleResponse(response);
};

const deleteCompany = async id => {
    let response = await fetch(
        `${API_URL}/companies/${id}`,
        {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${Auth.getToken()}`
            }
        }
    );
    Util.handleResponse(response);
};

export default {
    login,
    getCompanies,
    postFile,
    postCompany,
    deleteCompany
}
