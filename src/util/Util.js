import Auth from './Auth';

// Response and error handlers

function handleResponse(response) {
    if (response.status >= 401 && response.status < 500) {
        Auth.clearAuthentication();
        throw new Error('Unauthorized');
    }
}

async function handleJsonResponse(response) {
    if (response.status >= 401 && response.status < 500) {
        Auth.clearAuthentication();
        throw new Error('Unauthorized');
    }
    return await response.json();
}

function handleError(e, unauthorizedCallback) {
    console.log('Error occurred', e);
    if (e.message === 'Unauthorized') {
        unauthorizedCallback();
    }
}

export default {
    handleResponse,
    handleJsonResponse,
    handleError
};