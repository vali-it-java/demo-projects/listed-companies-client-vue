import { createRouter, createWebHashHistory } from 'vue-router'
import Main from '../components/Main.vue'
import Login from '../components/Login.vue'

const routes = [
  {
    path: "/",
    name: "Listed Companies",
    component: Main
  },
  {
    path: "/login",
    name: "Login",
    component: Login 
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
