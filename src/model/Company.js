export default class Company {
    constructor(
        id = 0,
        name = null,
        logo = null,
        established = null,
        employees = null,
        revenue = null,
        netIncome = null,
        securities = null,
        securityPrice = null,
        dividends = null
    ) {
        this.id = id;
        this.name = name !== '' ? name : null;
        this.logo = logo !== '' ? logo : null;
        this.established = established !== '' ? established : null;
        this.employees = employees !== '' ? employees : null;
        this.revenue = revenue !== '' ? revenue : null;
        this.netIncome = netIncome !== '' ? netIncome : null;
        this.securities = securities !== '' ? securities : null;
        this.securityPrice = securityPrice !== '' ? securityPrice : null;
        this.dividends = dividends !== '' ? dividends : null;
    }
}